#language: es
#encoding: UTF-8

Característica:Yo como cliente del almacen demoblaze
  Necesito visualizar los precios de los productos de la categoria Laptops
  Para tomar decisiones de compra

  Antecedentes:
    Dado El usuario se encuentra en la pagina principal de la pagina DemoBlaze
    Cuando El usuario ingrese a la categoria de laptops

  Escenario: Visualizar el precio de los productos
    Entonces se validara precio la categoria de laptops

  Escenario: Visualizar el nombre precio descripcion de un producto
      Entonces se validara la informacion de un producto de la categoria laptop
        | Nombre       | Precio | Descripcion                                                                      |
        | Sony vaio i5 | $790   | Sony is so confident that the VAIO S is a superior ultraportable laptop that the |

  Escenario: Validar la exitencia de la lista de productos
    Entonces se validara la existencia de los productos con su nombre precio descripcion

  Escenario: Validar la navegabilidad dentro de la categoria de laptops
    Cuando El usuario navegue entre las paginas de la categoria laptops
    Entonces Validar la navegabilidad dentro de la categoria de laptops