package co.com.tuya.certification.demoblaze.stepdefinitions;

import co.com.tuya.certification.demoblaze.questions.ValidarNavegabilidad;
import co.com.tuya.certification.demoblaze.tasks.NavegabilidadLaptops;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.thucydides.core.annotations.Managed;
import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;
import co.com.tuya.certification.demoblaze.questions.ValidarInformacion;
import co.com.tuya.certification.demoblaze.questions.ValidarListaProductos;
import co.com.tuya.certification.demoblaze.questions.ValidarPrecio;
import co.com.tuya.certification.demoblaze.tasks.Productos;

import java.util.List;
import java.util.Map;

import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class ProductosStepDefinitions {

    @Managed(driver="chrome")
    WebDriver hisdriver;

    @Before
    public void setThestage(){
        OnStage.setTheStage(new OnlineCast());
        OnStage.theActorCalled("User");
    }

    @Dado("^El usuario se encuentra en la pagina principal de la pagina DemoBlaze$")
    public void elUsuarioSeEncuentraEnLaPaginaPrincipalDeLaPaginaDemoBlaze() {
        theActorInTheSpotlight().wasAbleTo(Open.url("https://www.demoblaze.com/index.html"));
    }

    @Cuando("^El usuario ingrese a la categoria de laptops$")
    public void elUsuarioIngreseALaCategoriaDeLaptops() {
        theActorInTheSpotlight().attemptsTo(Productos.productos());
    }

    @Entonces("^se validara precio la categoria de laptops$")
    public void seValidaraPrecioLaCategoriaDeLaptops() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ValidarPrecio.validarPrecio(), Matchers.is(true)));
    }

    @Entonces("^se validara la informacion de un producto de la categoria laptop$")
    public void seValidaraLaInformacionDeUnProductoDeLaCategoriaLaptop(List<Map<String,String>> informacion) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ValidarInformacion.validarInformacion(informacion), Matchers.is(true)));
    }

    @Entonces("^se validara la existencia de los productos con su nombre precio descripcion$")
    public void seValidaraLaExistenciaDeLosProductosConSuNombrePrecioDescripcion() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ValidarListaProductos.validarListaProductos(), Matchers.is(true)));
    }

    @Cuando("^El usuario navegue entre las paginas de la categoria laptops$")
    public void elUsuarioNavegueEntreLasPaginasDeLaCategoriaLaptops() {
        theActorInTheSpotlight().attemptsTo(NavegabilidadLaptops.navegabilidadLaptops());
    }

    @Entonces("^Validar la navegabilidad dentro de la categoria de laptops$")
    public void validarLaNavegabilidadDentroDeLaCategoriaDeLaptops() {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(ValidarNavegabilidad.validarNavegabilidad(), Matchers.is(true)));
    }
}
