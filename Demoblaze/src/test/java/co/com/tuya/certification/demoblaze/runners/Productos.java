package co.com.tuya.certification.demoblaze.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/productos.feature",
glue = "co.com.tuya.certification.demoblaze.stepdefinitions",
snippets = SnippetType.CAMELCASE)
public class Productos {
}
