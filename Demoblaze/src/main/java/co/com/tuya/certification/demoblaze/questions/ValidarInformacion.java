package co.com.tuya.certification.demoblaze.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import java.util.List;
import java.util.Map;

import static co.com.tuya.certification.demoblaze.enums.ValidarInformacion.*;
import static co.com.tuya.certification.demoblaze.userinterfaces.PaginaLaptop.*;
import static co.com.tuya.certification.demoblaze.userinterfaces.PaginaLaptop.PRECIO_SONY;

public class ValidarInformacion implements Question {
    private List<Map<String,String>> informacion;
    public ValidarInformacion(List<Map<String,String>> informacion)
    {this.informacion = informacion;
    }

    @Override
    public Boolean answeredBy(Actor actor){
        boolean resultado = false;
        String TextoCompleto = NOMBRE_SONY.resolveFor(actor).getText() + PRECIO_SONY.of(String.valueOf(1)).resolveFor(actor).getText() + DESCRIPCION_SONY.resolveFor(actor).getText();
        String TextoFeature = informacion.get(0).get(MENSAJE_NOMBRE.getMensajeDemoBlaze())+ informacion.get(0).get(MENSAJE_PRECIO.getMensajeDemoBlaze()) +informacion.get(0).get(MENSAJE_DESCRIPCION.getMensajeDemoBlaze());
        resultado = TextoCompleto.contains(TextoFeature);
        return resultado;
    }
    public static ValidarInformacion validarInformacion(List<Map<String,String>> informacion){return new ValidarInformacion(informacion);}
}
