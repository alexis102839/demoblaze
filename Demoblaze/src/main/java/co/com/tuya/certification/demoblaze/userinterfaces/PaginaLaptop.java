package co.com.tuya.certification.demoblaze.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class PaginaLaptop {
    public static final Target NOMBRE_SONY = Target.the("Nombre del Sony vaio i5").
            locatedBy("//a[contains(text(),'Sony vaio i5')]");
    public static final Target PRECIO_SONY = Target.the("Precio del Sony vaio i5").
            locatedBy("(//h5[contains(text(),'$')])[{0}]");
    public static final Target DESCRIPCION_SONY = Target.the("Descripcion del Sony vaio i5").
            locatedBy("//a[contains(text(),'Sony vaio i5')] //ancestor::div[1]//p");
    public static final Target LISTA_PRODUCTOS = Target.the("Informacion General de los productos").
            locatedBy("(//div[@class='card h-100']//following-sibling::div)[{0}]");
    public static final Target BOTON_SIGUIENTE = Target.the("Boton para pasar de pagina").
            locatedBy("//button[@id='next2']");
    public static final Target BOTON_ATRAS = Target.the("Boton para retroceder de pagina").
            locatedBy("//button[@id='prev2']");
}
