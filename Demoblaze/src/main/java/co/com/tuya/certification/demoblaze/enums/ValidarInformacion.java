package co.com.tuya.certification.demoblaze.enums;

public enum ValidarInformacion {

    MENSAJE_NOMBRE("Nombre"),
    MENSAJE_PRECIO("Precio"),
    MENSAJE_DESCRIPCION("Descripcion");

    public String getMensajeDemoBlaze() {
        return MensajeDemoBlaze;
    }

    String MensajeDemoBlaze;

    ValidarInformacion(String MensajeDemoBlaze) {
        this.MensajeDemoBlaze = MensajeDemoBlaze;
    }
}
