package co.com.tuya.certification.demoblaze.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static co.com.tuya.certification.demoblaze.userinterfaces.PaginaPrincipal.*;

public class Productos implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(LAPTOP)
        );
    }
    public static Productos productos(){return instrumented(Productos.class);}
}
