package co.com.tuya.certification.demoblaze.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import java.util.ArrayList;
import java.util.List;

import static co.com.tuya.certification.demoblaze.enums.ValidarInformacion.MENSAJE_NOMBRE;
import static co.com.tuya.certification.demoblaze.userinterfaces.PaginaLaptop.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class NavegabilidadLaptops implements Task {
    ArrayList<String> nombreProducto = new ArrayList<String>();
    @Override
    public <T extends Actor> void performAs(T actor) {
        nombreProducto.add(NOMBRE_SONY.resolveFor(actor).getText());
        actor.attemptsTo(
                Click.on(BOTON_SIGUIENTE),
                Click.on(BOTON_ATRAS)
        );
        nombreProducto.add(NOMBRE_SONY.resolveFor(actor).getText());
        actor.remember(MENSAJE_NOMBRE.getMensajeDemoBlaze(),nombreProducto);
    }
    public static NavegabilidadLaptops navegabilidadLaptops(){return instrumented(NavegabilidadLaptops.class);}
}
