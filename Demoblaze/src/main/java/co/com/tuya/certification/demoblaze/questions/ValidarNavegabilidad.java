package co.com.tuya.certification.demoblaze.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import java.util.List;

import static co.com.tuya.certification.demoblaze.enums.ValidarInformacion.MENSAJE_NOMBRE;

public class ValidarNavegabilidad implements Question {
    @Override
    public Object answeredBy(Actor actor) {
        List<String> nombres = actor.recall(MENSAJE_NOMBRE.getMensajeDemoBlaze());
        if (nombres.get(0).equals(nombres.get(1))){
            return true;
        }else{
            return false;
        }
    }
    public static ValidarNavegabilidad validarNavegabilidad(){return new ValidarNavegabilidad();}
}
