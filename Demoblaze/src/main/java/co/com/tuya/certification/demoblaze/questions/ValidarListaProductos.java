package co.com.tuya.certification.demoblaze.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import static co.com.tuya.certification.demoblaze.userinterfaces.PaginaLaptop.*;

public class ValidarListaProductos implements Question {
    @Override
    public Boolean answeredBy(Actor actor){
        boolean resultado = false;
        for (int i =1;i<=6;i++){
            resultado=LISTA_PRODUCTOS.of(String.valueOf(i)).resolveFor(actor).isPresent();
        }
        return resultado;
    }
    public static ValidarListaProductos validarListaProductos(){return new ValidarListaProductos();}
}
