package co.com.tuya.certification.demoblaze.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class PaginaPrincipal {
    public static final Target LAPTOP = Target.the("Boton categoria laptops").
            locatedBy("//a[@id='itemc'][2]");
}
