package co.com.tuya.certification.demoblaze.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.tuya.certification.demoblaze.userinterfaces.PaginaLaptop.PRECIO_SONY;

public class ValidarPrecio implements Question {
    @Override
    public Boolean answeredBy(Actor actor) {
        boolean resultado = false;
        for (int i =1;i<=6;i++){
            resultado=PRECIO_SONY.of(String.valueOf(i)).resolveFor(actor).isPresent();
        }
        return resultado;
    }
    public static ValidarPrecio validarPrecio(){return new ValidarPrecio();}
}
